import request from '@/utils/request'

// api地址
const api = {
  data: 'setting/data',
  system: 'system/config',
  store: 'store/list',
}

// 设置项详情
export function data() {
  return request.get(api.data)
}

// 系统配置
export function systemConfig() {
  return request.get(api.system)
}

// 店铺列表
export const storeList = (keyword) => {
  return request.post(api.store, { keyword })
}
