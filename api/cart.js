import request from '@/utils/request'

// api地址
const api = {
  list: 'cart/list',
  save: 'cart/save',
  clear: 'cart/clear',
}

// 购物车列表
export const list = (cartIds, goodsId, skuId, buyNum, couponId, point) => {
  return request.post(api.list, { cartIds, goodsId, skuId, buyNum, couponId, point })
}

// 更新购物车
export const save = (goodsId, action, skuId, buyNum) => {
  return request.post(api.save, { goodsId, action, skuId, buyNum })
}

// 删除购物车商品
export const clear = (cartId) => {
  return request.post(api.clear, { cartId })
}
